//
//  NetworkManagerTests.swift
//  SeatGeekDemoTests
//
//  Created by Kevin Mitchell on 4/28/21.
//

import XCTest
@testable import SeatGeekDemo

class NetworkManagerTests: XCTestCase {
    
    class NetworkManagerMockWithEventsResponse: NetworkEngine {
        func performRequest(for url: URL, method: HTTPMethods, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
            let data = EventsResponseMock.data(using: .utf8)
            completion(data, nil, nil)
        }
    }
    
    class NetworkManagerMockWithEventResponse: NetworkEngine {
        func performRequest(for url: URL, method: HTTPMethods, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
            let data = SingleEventResponseMock.data(using: .utf8)
            completion(data, nil, nil)
        }
    }
    
    class NetworkManagerMockWithInvalidJSON: NetworkEngine {
        func performRequest(for url: URL, method: HTTPMethods, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
            let data = InvalidJSONMock.data(using: .utf8)
            completion(data, nil, nil)
        }
    }
    
    func testGetEventsShouldSucceed() {
        let networkEngine = NetworkManagerMockWithEventsResponse()
        let networkManager = NetworkManager(networkEngine: networkEngine)
        
        var result: Result<[Event], Error>?
        networkManager.getEvents { result = $0 }
        
        let mockEventsValue = [Event(id: 801255,
                               datetimeUTC: "2012-02-13T16:00:00",
                               venue: Venue(nameV2: "Rock N Soul Museum",
                                            displayLocation: "Memphis, TN"),
                               performers: [Performer(image: "https://seatgeek.com/images/performers-landscape/memphis-rock-n-soul-museum-25f34d/13386/huge.jpg")]),
                         Event(id: 801255,
                               datetimeUTC: "2012-02-13T16:00:00",
                               venue: Venue(nameV2: "Rock N Soul Museum",
                                            displayLocation: "Memphis, TN"),
                                performers: [Performer(image: "https://seatgeek.com/images/performers-landscape/memphis-rock-n-soul-museum-25f34d/13386/huge.jpg")]),
                         Event(id: 801255,
                               datetimeUTC: "2012-02-13T16:00:00",
                               venue: Venue(nameV2: "Rock N Soul Museum",
                                            displayLocation: "Memphis, TN"),
                               performers: [Performer(image: "https://seatgeek.com/images/performers-landscape/memphis-rock-n-soul-museum-25f34d/13386/huge.jpg")])]
        
        switch result {
        case .success(let value):
            XCTAssertEqual(value, mockEventsValue)
        default:
            XCTFail("Data is nil or not properly formatted")
        }
    }
    
    func testGetEventsShouldNotSucceed() {
        let networkEngine = NetworkManagerMockWithInvalidJSON()
        let networkManager = NetworkManager(networkEngine: networkEngine)
        
        var result: Result<[Event], Error>?
        networkManager.getEvents { result = $0 }
        
        switch result {
        case .failure(let error):
            XCTAssertEqual(error.localizedDescription,
                           "The data couldn’t be read because it isn’t in the correct format.")
        default:
            XCTFail("Data is nil or properly formatted")
        }
    }
    
    func testGetEventShouldSucceed() {
        let networkEngine = NetworkManagerMockWithEventResponse()
        let networkManager = NetworkManager(networkEngine: networkEngine)
        
        var result: Result<Event, Error>?
        networkManager.getEvent(with: 801255) { result = $0 }
        
        let mockEvent = Event(id: 801255,
                              datetimeUTC: "2012-02-13T16:00:00",
                              venue: Venue(nameV2: "Rock N Soul Museum",
                                           displayLocation: "Memphis, TN"),
                               performers: [Performer(image: "https://seatgeek.com/images/performers-landscape/memphis-rock-n-soul-museum-25f34d/13386/huge.jpg")])
        
        switch result {
        case .success(let value):
            XCTAssertEqual(value, mockEvent)
        default:
            XCTFail("Data is nil or not properly formatted")
        }
    }
    
    func testGetEventShouldNotSucceed() {
        let networkEngine = NetworkManagerMockWithInvalidJSON()
        let networkManager = NetworkManager(networkEngine: networkEngine)
        
        var result: Result<Event, Error>?
        networkManager.getEvent(with: 1234) { result = $0 }
        
        switch result {
        case .failure(let error):
            XCTAssertEqual(error.localizedDescription,
                           "The data couldn’t be read because it isn’t in the correct format.")
        default:
            XCTFail("Data is nil or properly formatted")
        }
    }
    
}
