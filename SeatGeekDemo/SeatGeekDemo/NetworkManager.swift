//
//  NetworkManager.swift
//  SeatGeekDemo
//
//  Created by Kevin Mitchell on 4/28/21.
//

import Foundation

enum HTTPMethods: String {
    case GET
    case POST
}

protocol NetworkEngine {
    func performRequest(for url: URL, method: HTTPMethods, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: NetworkEngine {
    //Add xcode generated comments here
    func performRequest(for url: URL,
                        method: HTTPMethods,
                        completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        //make a URLRequest with basic authentication enabled in the header
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue("Basic MjE4MDg1OTl8MTYxOTY0Mjk2Mi4xNzIwMzc", forHTTPHeaderField: "Authorization")
        
        let task = dataTask(with: request, completionHandler: completion)
        task.resume()
    }
}

protocol NetworkManagerActions {
    func getEvents(completion: @escaping (Result<[Event], Error>) -> Void)
    func getEvents(with query: String, completion: @escaping (Result<[Event], Error>) -> Void)
    func getEvent(with id: Int, completion: @escaping (Result<Event, Error>) -> Void)
}

public class NetworkManager: NetworkManagerActions {
    private var networkEngine: NetworkEngine
    
    init(networkEngine: NetworkEngine = URLSession.shared) {
        self.networkEngine = networkEngine
    }
    
    //Add xcode generated comments here
    func getEvents(completion: @escaping (Result<[Event], Error>) -> Void) {
        guard let eventsURL = URL(string: "https://api.seatgeek.com/2/events") else { return }
        
        networkEngine.performRequest(for: eventsURL, method: .GET) { (data, response, error) in
            if let error = error { completion(.failure(error)) }
            guard let data = data else { return }

            do {
                let events = try JSONDecoder().decode([Event].self, from: data)
                completion(.success(events))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    //Add xcode generated comments here
    func getEvents(with query: String, completion: @escaping (Result<[Event], Error>) -> Void) {
        
    }
    
    //Add xcode generated comments here
    func getEvent(with id: Int, completion: @escaping (Result<Event, Error>) -> Void) {
        guard let eventsURL = URL(string: "https://api.seatgeek.com/2/events/\(id)") else { return }
        
        networkEngine.performRequest(for: eventsURL, method: .GET) { (data, response, error) in
            if let error = error { completion(.failure(error)) }
            guard let data = data else { return }

            do {
                let event = try JSONDecoder().decode(Event.self, from: data)
                completion(.success(event))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
