//
//  EventModel.swift
//  SeatGeekDemo
//
//  Created by Kevin Mitchell on 4/28/21.
//

import Foundation

//explain why to use struct vs class here
//use Decodable insead of codeable here
// MARK: - Event
struct Event: Codable, Equatable {
    let id: Int
    let datetimeUTC: String
    let venue: Venue
    let performers: [Performer]

    enum CodingKeys: String, CodingKey {
        case id
        case datetimeUTC = "datetime_utc"
        case venue, performers
    }
}

// MARK: - Performer
struct Performer: Codable, Equatable {
    let image: String
}

// MARK: - Venue
struct Venue: Codable, Equatable {
    let nameV2, displayLocation: String

    enum CodingKeys: String, CodingKey {
        case nameV2 = "name_v2"
        case displayLocation = "display_location"
    }
}
